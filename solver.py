from pwn import *
import string

context.log_level = "error"

def get_full(inp):
	r = process(["bash", "-c", f"public/papa_bear '{inp}' 0>&1"])
	mustache = r.recvall().decode()
	bits = ''
	for c in mustache:
		if c == 'M':
			bits += '0'
		if c == 'W':
			bits += '1'
	r.close()
	return bits

a_encoding = get_full('a')[:10]
assert(a_encoding.count('1'))

lut = {}
for c in string.printable:
	e = get_full(c+'a')
	ai = e.rfind(a_encoding)
	lut[e[:ai]] = c
	print(f"{repr(c)} {e[:ai]}")

flag_mustache = open("public/description.txt", "r").read()
bits = ''
for c in flag_mustache:
	if c == 'M':
		bits += '0'
	if c == 'W':
		bits += '1'

print(bits)
flag = ""
B = ""
for c in bits:
	B += c
	if B in lut:
		flag += lut[B]
		B = ""

print(flag)
