
section .vmp2 nowrite exec
global twine0_start
extern ohno

%macro myjmp 1
	jmp %1
	align 2
%endmacro

TWINE_0_2 equ $
ok74: mov al, 13
myjmp TWINE_0_1
TWINE_1_2 equ $
ok89: mov al, 60
myjmp TWINE_1_1
TWINE_2_2 equ $
ok51: mov al, 27
myjmp TWINE_2_1
TWINE_0_1 equ $
mov cl, 7
myjmp TWINE_0_0
TWINE_1_1 equ $
mov cl, 6
myjmp TWINE_1_0
TWINE_2_1 equ $
mov cl, 6
myjmp TWINE_2_0
TWINE_0_0 equ $
ret
TWINE_1_0 equ $
ret
TWINE_2_0 equ $
ret
TWINE_3_2 equ $
ok62: mov al, 6
myjmp TWINE_3_1
TWINE_4_2 equ $
ok70: mov al, 31
myjmp TWINE_4_1
TWINE_5_28 equ $
twine3_start: xor al, 112^255
myjmp TWINE_5_27
TWINE_3_1 equ $
mov cl, 7
myjmp TWINE_3_0
TWINE_4_1 equ $
mov cl, 7
myjmp TWINE_4_0
TWINE_5_27 equ $
inc al
myjmp TWINE_5_26
TWINE_3_0 equ $
ret
TWINE_4_0 equ $
ret
TWINE_5_26 equ $
jz ok112
myjmp TWINE_5_25
TWINE_5_25 equ $
dec al
myjmp TWINE_5_24
TWINE_6_2 equ $
ok49: mov al, 86
myjmp TWINE_6_1
TWINE_7_2 equ $
ok106: mov al, 74
myjmp TWINE_7_1
TWINE_5_24 equ $
xor al, 112^255
myjmp TWINE_5_23
TWINE_6_1 equ $
mov cl, 7
myjmp TWINE_6_0
TWINE_7_1 equ $
mov cl, 7
myjmp TWINE_7_0
TWINE_5_23 equ $
fallthrough112:rol al, 3
myjmp TWINE_5_22
TWINE_6_0 equ $
ret
TWINE_7_0 equ $
ret
TWINE_5_22 equ $
cmp al, ((113<<3) | (113>>5)) & 255
myjmp TWINE_5_21
TWINE_5_21 equ $
jz ok113
myjmp TWINE_5_20
TWINE_8_2 equ $
ok124: mov al, 17
myjmp TWINE_8_1
TWINE_9_2 equ $
ok91: mov al, 9
myjmp TWINE_9_1
TWINE_5_20 equ $
ror al, 3
myjmp TWINE_5_19
TWINE_8_1 equ $
mov cl, 7
myjmp TWINE_8_0
TWINE_9_1 equ $
mov cl, 7
myjmp TWINE_9_0
TWINE_5_19 equ $
fallthrough113:mov cl, 195
myjmp TWINE_5_18
TWINE_8_0 equ $
ret
TWINE_9_0 equ $
ret
TWINE_5_18 equ $
mov r8b, al
myjmp TWINE_5_17
TWINE_5_17 equ $
mul cl
myjmp TWINE_5_16
TWINE_10_2 equ $
ok71: mov al, 70
myjmp TWINE_10_1
TWINE_11_2 equ $
ok46: mov al, 42
myjmp TWINE_11_1
TWINE_5_16 equ $
cmp ax, 195*82
myjmp TWINE_5_15
TWINE_10_1 equ $
mov cl, 7
myjmp TWINE_10_0
TWINE_11_1 equ $
mov cl, 7
myjmp TWINE_11_0
TWINE_5_15 equ $
jz ok82
myjmp TWINE_5_14
TWINE_10_0 equ $
ret
TWINE_11_0 equ $
ret
TWINE_5_14 equ $
mov al, r8b
myjmp TWINE_5_13
TWINE_5_13 equ $
fallthrough82:rol al, 3
myjmp TWINE_5_12
TWINE_12_31 equ $
twine7_start: add al, 256-108
myjmp TWINE_12_30
TWINE_13_2 equ $
ok10: mov al, 41
myjmp TWINE_13_1
TWINE_5_12 equ $
cmp al, ((89<<3) | (89>>5)) & 255
myjmp TWINE_5_11
TWINE_12_30 equ $
jz ok108
 add al, 108
myjmp TWINE_12_29
TWINE_13_1 equ $
mov cl, 7
myjmp TWINE_13_0
TWINE_5_11 equ $
jz ok89
myjmp TWINE_5_10
TWINE_12_29 equ $
fallthrough108:xor ah, ah
myjmp TWINE_12_28
TWINE_13_0 equ $
ret
TWINE_5_10 equ $
ror al, 3
myjmp TWINE_5_9
TWINE_12_28 equ $
mov r8w, ax
myjmp TWINE_12_27
TWINE_5_9 equ $
fallthrough89:xor al, 90^255
myjmp TWINE_5_8
TWINE_12_27 equ $

myjmp TWINE_12_26
TWINE_14_51 equ $
twine14_start: xor al, 49
myjmp TWINE_14_50
TWINE_5_8 equ $
inc al
myjmp TWINE_5_7
TWINE_12_26 equ $
bsf cx, r8w
myjmp TWINE_12_25
TWINE_14_50 equ $
jz ok49
 xor al, 49
myjmp TWINE_14_49
TWINE_5_7 equ $
jz ok90
myjmp TWINE_5_6
TWINE_12_25 equ $
shr r8w, 0+1
myjmp TWINE_12_24
TWINE_14_49 equ $
fallthrough49:xor rcx, rcx
 mov cl, 105
myjmp TWINE_14_48
TWINE_5_6 equ $
dec al
myjmp TWINE_5_5
TWINE_12_24 equ $
cmp cx, 0
myjmp TWINE_12_23
TWINE_14_48 equ $
lp105: dec al
myjmp TWINE_14_47
TWINE_5_5 equ $
xor al, 90^255
myjmp TWINE_5_4
TWINE_12_23 equ $
jnz fallthrough43
myjmp TWINE_12_22
TWINE_14_47 equ $
loop lp105
myjmp TWINE_14_46
TWINE_5_4 equ $
fallthrough90:sub al, 9
myjmp TWINE_5_3
TWINE_12_22 equ $

myjmp TWINE_12_21
TWINE_14_46 equ $
test al, al
myjmp TWINE_14_45
TWINE_5_3 equ $
test al, al
myjmp TWINE_5_2
TWINE_12_21 equ $
bsf cx, r8w
myjmp TWINE_12_20
TWINE_14_45 equ $
jz ok105
myjmp TWINE_14_44
TWINE_5_2 equ $
jz ok9
myjmp TWINE_5_1
TWINE_12_20 equ $
shr r8w, 0+1
myjmp TWINE_12_19
TWINE_14_44 equ $
add al, 105
myjmp TWINE_14_43
TWINE_5_1 equ $
add al, 9
myjmp TWINE_5_0
TWINE_12_19 equ $
cmp cx, 0
myjmp TWINE_12_18
TWINE_14_43 equ $
fallthrough105:rol al, 3
myjmp TWINE_14_42
TWINE_5_0 equ $
fallthrough9:jmp twine4_start
TWINE_12_18 equ $
jnz fallthrough43
myjmp TWINE_12_17
TWINE_14_42 equ $
cmp al, ((116<<3) | (116>>5)) & 255
myjmp TWINE_14_41
TWINE_12_17 equ $

myjmp TWINE_12_16
TWINE_14_41 equ $
jz ok116
myjmp TWINE_14_40
TWINE_15_2 equ $
ok32: mov al, 7
myjmp TWINE_15_1
TWINE_12_16 equ $
bsf cx, r8w
myjmp TWINE_12_15
TWINE_14_40 equ $
ror al, 3
myjmp TWINE_14_39
TWINE_15_1 equ $
mov cl, 5
myjmp TWINE_15_0
TWINE_12_15 equ $
shr r8w, 1+1
myjmp TWINE_12_14
TWINE_14_39 equ $
fallthrough116:xor rcx, rcx
 mov cl, 117
myjmp TWINE_14_38
TWINE_15_0 equ $
ret
TWINE_12_14 equ $
cmp cx, 1
myjmp TWINE_12_13
TWINE_14_38 equ $
lp117: dec al
myjmp TWINE_14_37
TWINE_12_13 equ $
jnz fallthrough43
myjmp TWINE_12_12
TWINE_14_37 equ $
loop lp117
myjmp TWINE_14_36
TWINE_16_2 equ $
ok41: mov al, 78
myjmp TWINE_16_1
TWINE_12_12 equ $

myjmp TWINE_12_11
TWINE_14_36 equ $
test al, al
myjmp TWINE_14_35
TWINE_16_1 equ $
mov cl, 7
myjmp TWINE_16_0
TWINE_12_11 equ $
bsf cx, r8w
myjmp TWINE_12_10
TWINE_14_35 equ $
jz ok117
myjmp TWINE_14_34
TWINE_16_0 equ $
ret
TWINE_12_10 equ $
shr r8w, 1+1
myjmp TWINE_12_9
TWINE_14_34 equ $
add al, 117
myjmp TWINE_14_33
TWINE_12_9 equ $
cmp cx, 1
myjmp TWINE_12_8
TWINE_14_33 equ $
fallthrough117:bt ax, 0
myjmp TWINE_14_32
TWINE_17_2 equ $
ok58: mov al, 2
myjmp TWINE_17_1
TWINE_12_8 equ $
jnz fallthrough43
myjmp TWINE_12_7
TWINE_14_32 equ $
jnc fallthrough121
myjmp TWINE_14_31
TWINE_17_1 equ $
mov cl, 6
myjmp TWINE_17_0
TWINE_12_7 equ $
test r8w, r8w
myjmp TWINE_12_6
TWINE_14_31 equ $
bt ax, 1
myjmp TWINE_14_30
TWINE_17_0 equ $
ret
TWINE_12_6 equ $
jz ok43
myjmp TWINE_12_5
TWINE_14_30 equ $
jc fallthrough121
myjmp TWINE_14_29
TWINE_12_5 equ $
fallthrough43:xor al, 32^255
myjmp TWINE_12_4
TWINE_14_29 equ $
bt ax, 2
myjmp TWINE_14_28
TWINE_18_2 equ $
ok104: mov al, 24
myjmp TWINE_18_1
TWINE_12_4 equ $
inc al
myjmp TWINE_12_3
TWINE_14_28 equ $
jc fallthrough121
myjmp TWINE_14_27
TWINE_18_1 equ $
mov cl, 6
myjmp TWINE_18_0
TWINE_12_3 equ $
jz ok32
myjmp TWINE_12_2
TWINE_14_27 equ $
bt ax, 3
myjmp TWINE_14_26
TWINE_18_0 equ $
ret
TWINE_12_2 equ $
dec al
myjmp TWINE_12_1
TWINE_14_26 equ $
jnc fallthrough121
myjmp TWINE_14_25
TWINE_12_1 equ $
xor al, 32^255
myjmp TWINE_12_0
TWINE_14_25 equ $
bt ax, 4
myjmp TWINE_14_24
TWINE_19_2 equ $
ok44: mov al, 81
myjmp TWINE_19_1
TWINE_12_0 equ $
fallthrough32:jmp twine8_start
TWINE_14_24 equ $
jnc fallthrough121
myjmp TWINE_14_23
TWINE_19_1 equ $
mov cl, 7
myjmp TWINE_19_0
TWINE_14_23 equ $
bt ax, 5
myjmp TWINE_14_22
TWINE_19_0 equ $
ret
TWINE_20_2 equ $
ok35: mov al, 21
myjmp TWINE_20_1
TWINE_14_22 equ $
jnc fallthrough121
myjmp TWINE_14_21
TWINE_20_1 equ $
mov cl, 7
myjmp TWINE_20_0
TWINE_14_21 equ $
bt ax, 6
myjmp TWINE_14_20
TWINE_21_2 equ $
ok99: mov al, 56
myjmp TWINE_21_1
TWINE_20_0 equ $
ret
TWINE_14_20 equ $
jnc fallthrough121
myjmp TWINE_14_19
TWINE_21_1 equ $
mov cl, 6
myjmp TWINE_21_0
TWINE_14_19 equ $
jmp ok121
myjmp TWINE_14_18
TWINE_21_0 equ $
ret
TWINE_22_2 equ $
ok80: mov al, 53
myjmp TWINE_22_1
TWINE_14_18 equ $
fallthrough121:test al, 255^86
myjmp TWINE_14_17
TWINE_22_1 equ $
mov cl, 7
myjmp TWINE_22_0
TWINE_14_17 equ $
jnz fallthrough86
myjmp TWINE_14_16
TWINE_23_2 equ $
ok87: mov al, 113
myjmp TWINE_23_1
TWINE_22_0 equ $
ret
TWINE_14_16 equ $
mov r8b, al
myjmp TWINE_14_15
TWINE_23_1 equ $
mov cl, 7
myjmp TWINE_23_0
TWINE_14_15 equ $
or al, 255^86
myjmp TWINE_14_14
TWINE_23_0 equ $
ret
TWINE_24_2 equ $
ok9: mov al, 57
myjmp TWINE_24_1
TWINE_14_14 equ $
cmp al, 255
myjmp TWINE_14_13
TWINE_24_1 equ $
mov cl, 7
myjmp TWINE_24_0
TWINE_14_13 equ $
jz ok86
myjmp TWINE_14_12
TWINE_25_2 equ $
ok77: mov al, 59
myjmp TWINE_25_1
TWINE_24_0 equ $
ret
TWINE_14_12 equ $
mov al, r8b
myjmp TWINE_14_11
TWINE_25_1 equ $
mov cl, 6
myjmp TWINE_25_0
TWINE_14_11 equ $
fallthrough86:xor rcx, rcx
 xchg rax, rdx
myjmp TWINE_14_10
TWINE_25_0 equ $
ret
TWINE_26_2 equ $
ok45: mov al, 118
myjmp TWINE_26_1
TWINE_14_10 equ $
mov rax, 1477399826
myjmp TWINE_14_9
TWINE_26_1 equ $
mov cl, 7
myjmp TWINE_26_0
TWINE_14_9 equ $
xchg rax, rdx
myjmp TWINE_14_8
TWINE_27_20 equ $
twine8_start: xor rcx, rcx
 xchg rax, rdx
myjmp TWINE_27_19
TWINE_26_0 equ $
ret
TWINE_14_8 equ $
crc32 rcx, al
myjmp TWINE_14_7
TWINE_27_19 equ $
mov rax, 4215344322
myjmp TWINE_27_18
TWINE_14_7 equ $
cmp rcx, rdx
 jz ok41
myjmp TWINE_14_6
TWINE_27_18 equ $
xchg rax, rdx
myjmp TWINE_27_17
TWINE_28_2 equ $
ok111: mov al, 11
myjmp TWINE_28_1
TWINE_14_6 equ $
fallthrough41:cmp al, 60
myjmp TWINE_14_5
TWINE_27_17 equ $
crc32 rcx, al
myjmp TWINE_27_16
TWINE_28_1 equ $
mov cl, 7
myjmp TWINE_28_0
TWINE_14_5 equ $
jz ok60
myjmp TWINE_14_4
TWINE_27_16 equ $
cmp rcx, rdx
 jz ok120
myjmp TWINE_27_15
TWINE_28_0 equ $
ret
TWINE_14_4 equ $
fallthrough60:sub al, 93
myjmp TWINE_14_3
TWINE_27_15 equ $
fallthrough120:xor al, 33
myjmp TWINE_27_14
TWINE_14_3 equ $
test al, al
myjmp TWINE_14_2
TWINE_27_14 equ $
jz ok33
 xor al, 33
myjmp TWINE_27_13
TWINE_29_2 equ $
ok98: mov al, 55
myjmp TWINE_29_1
TWINE_14_2 equ $
jz ok93
myjmp TWINE_14_1
TWINE_27_13 equ $
fallthrough33:xor rcx, rcx
 mov cl, 34
myjmp TWINE_27_12
TWINE_29_1 equ $
mov cl, 6
myjmp TWINE_29_0
TWINE_14_1 equ $
add al, 93
myjmp TWINE_14_0
TWINE_27_12 equ $
lp34: dec al
myjmp TWINE_27_11
TWINE_29_0 equ $
ret
TWINE_14_0 equ $
fallthrough93:jmp twine15_start
TWINE_27_11 equ $
loop lp34
myjmp TWINE_27_10
TWINE_27_10 equ $
test al, al
myjmp TWINE_27_9
TWINE_30_2 equ $
ok39: mov al, 5
myjmp TWINE_30_1
TWINE_31_25 equ $
twine13_start: bt ax, 0
myjmp TWINE_31_24
TWINE_27_9 equ $
jz ok34
myjmp TWINE_27_8
TWINE_30_1 equ $
mov cl, 7
myjmp TWINE_30_0
TWINE_31_24 equ $
jc fallthrough126
myjmp TWINE_31_23
TWINE_27_8 equ $
add al, 34
myjmp TWINE_27_7
TWINE_30_0 equ $
ret
TWINE_31_23 equ $
bt ax, 1
myjmp TWINE_31_22
TWINE_27_7 equ $
fallthrough34:test al, 255^35
myjmp TWINE_27_6
TWINE_31_22 equ $
jnc fallthrough126
myjmp TWINE_31_21
TWINE_27_6 equ $
jnz fallthrough35
myjmp TWINE_27_5
TWINE_32_2 equ $
ok126: mov al, 90
myjmp TWINE_32_1
TWINE_31_21 equ $
bt ax, 2
myjmp TWINE_31_20
TWINE_27_5 equ $
mov r8b, al
myjmp TWINE_27_4
TWINE_32_1 equ $
mov cl, 7
myjmp TWINE_32_0
TWINE_31_20 equ $
jnc fallthrough126
myjmp TWINE_31_19
TWINE_27_4 equ $
or al, 255^35
myjmp TWINE_27_3
TWINE_32_0 equ $
ret
TWINE_31_19 equ $
bt ax, 3
myjmp TWINE_31_18
TWINE_27_3 equ $
cmp al, 255
myjmp TWINE_27_2
TWINE_31_18 equ $
jnc fallthrough126
myjmp TWINE_31_17
TWINE_27_2 equ $
jz ok35
myjmp TWINE_27_1
TWINE_33_2 equ $
ok123: mov al, 4
myjmp TWINE_33_1
TWINE_31_17 equ $
bt ax, 4
myjmp TWINE_31_16
TWINE_27_1 equ $
mov al, r8b
myjmp TWINE_27_0
TWINE_33_1 equ $
mov cl, 6
myjmp TWINE_33_0
TWINE_31_16 equ $
jnc fallthrough126
myjmp TWINE_31_15
TWINE_27_0 equ $
fallthrough35:jmp twine9_start
TWINE_33_0 equ $
ret
TWINE_31_15 equ $
bt ax, 5
myjmp TWINE_31_14
TWINE_31_14 equ $
jnc fallthrough126
myjmp TWINE_31_13
TWINE_34_2 equ $
ok108: mov al, 65
myjmp TWINE_34_1
TWINE_35_2 equ $
ok60: mov al, 19
myjmp TWINE_35_1
TWINE_31_13 equ $
bt ax, 6
myjmp TWINE_31_12
TWINE_34_1 equ $
mov cl, 7
myjmp TWINE_34_0
TWINE_35_1 equ $
mov cl, 7
myjmp TWINE_35_0
TWINE_31_12 equ $
jnc fallthrough126
myjmp TWINE_31_11
TWINE_34_0 equ $
ret
TWINE_35_0 equ $
ret
TWINE_31_11 equ $
jmp ok126
myjmp TWINE_31_10
TWINE_31_10 equ $
fallthrough126:mov cl, 255^11
myjmp TWINE_31_9
TWINE_36_2 equ $
ok61: mov al, 75
myjmp TWINE_36_1
TWINE_37_2 equ $
ok65: mov al, 89
myjmp TWINE_37_1
TWINE_31_9 equ $
xor cl, al
myjmp TWINE_31_8
TWINE_36_1 equ $
mov cl, 7
myjmp TWINE_36_0
TWINE_37_1 equ $
mov cl, 7
myjmp TWINE_37_0
TWINE_31_8 equ $
inc cl
myjmp TWINE_31_7
TWINE_36_0 equ $
ret
TWINE_37_0 equ $
ret
TWINE_31_7 equ $
test cl, cl
myjmp TWINE_31_6
TWINE_31_6 equ $
jz ok11
myjmp TWINE_31_5
TWINE_38_2 equ $
ok64: mov al, 8
myjmp TWINE_38_1
TWINE_39_37 equ $
twine6_start: test al, 255^55
myjmp TWINE_39_36
TWINE_31_5 equ $
fallthrough11:mov cl, 255^12
myjmp TWINE_31_4
TWINE_38_1 equ $
mov cl, 6
myjmp TWINE_38_0
TWINE_39_36 equ $
jnz fallthrough55
myjmp TWINE_39_35
TWINE_31_4 equ $
xor cl, al
myjmp TWINE_31_3
TWINE_38_0 equ $
ret
TWINE_39_35 equ $
mov r8b, al
myjmp TWINE_39_34
TWINE_31_3 equ $
inc cl
myjmp TWINE_31_2
TWINE_39_34 equ $
or al, 255^55
myjmp TWINE_39_33
TWINE_31_2 equ $
test cl, cl
myjmp TWINE_31_1
TWINE_40_2 equ $
ok97: mov al, 47
myjmp TWINE_40_1
TWINE_39_33 equ $
cmp al, 255
myjmp TWINE_39_32
TWINE_31_1 equ $
jz ok12
myjmp TWINE_31_0
TWINE_40_1 equ $
mov cl, 6
myjmp TWINE_40_0
TWINE_39_32 equ $
jz ok55
myjmp TWINE_39_31
TWINE_31_0 equ $
fallthrough12:jmp twine14_start
TWINE_40_0 equ $
ret
TWINE_39_31 equ $
mov al, r8b
myjmp TWINE_39_30
TWINE_39_30 equ $
fallthrough55:mov cl, 255^103
myjmp TWINE_39_29
TWINE_41_2 equ $
ok56: mov al, 1
myjmp TWINE_41_1
TWINE_42_2 equ $
ok90: mov al, 109
myjmp TWINE_42_1
TWINE_39_29 equ $
xor cl, al
myjmp TWINE_39_28
TWINE_41_1 equ $
mov cl, 7
myjmp TWINE_41_0
TWINE_42_1 equ $
mov cl, 7
myjmp TWINE_42_0
TWINE_39_28 equ $
inc cl
myjmp TWINE_39_27
TWINE_41_0 equ $
ret
TWINE_42_0 equ $
ret
TWINE_39_27 equ $
test cl, cl
myjmp TWINE_39_26
TWINE_39_26 equ $
jz ok103
myjmp TWINE_39_25
TWINE_43_2 equ $
ok67: mov al, 26
myjmp TWINE_43_1
TWINE_44_2 equ $
ok82: mov al, 34
myjmp TWINE_44_1
TWINE_39_25 equ $
fallthrough103:rol al, 3
myjmp TWINE_39_24
TWINE_43_1 equ $
mov cl, 7
myjmp TWINE_43_0
TWINE_44_1 equ $
mov cl, 6
myjmp TWINE_44_0
TWINE_39_24 equ $
cmp al, ((111<<3) | (111>>5)) & 255
myjmp TWINE_39_23
TWINE_43_0 equ $
ret
TWINE_44_0 equ $
ret
TWINE_39_23 equ $
jz ok111
myjmp TWINE_39_22
TWINE_39_22 equ $
ror al, 3
myjmp TWINE_39_21
TWINE_45_2 equ $
ok107: mov al, 63
myjmp TWINE_45_1
TWINE_46_2 equ $
ok117: mov al, 85
myjmp TWINE_46_1
TWINE_39_21 equ $
fallthrough111:mov cl, 255^74
myjmp TWINE_39_20
TWINE_45_1 equ $
mov cl, 7
myjmp TWINE_45_0
TWINE_46_1 equ $
mov cl, 7
myjmp TWINE_46_0
TWINE_39_20 equ $
xor cl, al
myjmp TWINE_39_19
TWINE_45_0 equ $
ret
TWINE_46_0 equ $
ret
TWINE_39_19 equ $
inc cl
myjmp TWINE_39_18
TWINE_39_18 equ $
test cl, cl
myjmp TWINE_39_17
TWINE_47_2 equ $
ok36: mov al, 0
myjmp TWINE_47_1
TWINE_48_2 equ $
ok11: mov al, 125
myjmp TWINE_48_1
TWINE_39_17 equ $
jz ok74
myjmp TWINE_39_16
TWINE_47_1 equ $
mov cl, 6
myjmp TWINE_47_0
TWINE_48_1 equ $
mov cl, 7
myjmp TWINE_48_0
TWINE_39_16 equ $
fallthrough74:mov cl, 255^37
myjmp TWINE_39_15
TWINE_47_0 equ $
ret
TWINE_48_0 equ $
ret
TWINE_39_15 equ $
xor cl, al
myjmp TWINE_39_14
TWINE_39_14 equ $
inc cl
myjmp TWINE_39_13
TWINE_49_2 equ $
ok78: mov al, 77
myjmp TWINE_49_1
TWINE_50_2 equ $
ok120: mov al, 46
myjmp TWINE_50_1
TWINE_39_13 equ $
test cl, cl
myjmp TWINE_39_12
TWINE_49_1 equ $
mov cl, 7
myjmp TWINE_49_0
TWINE_50_1 equ $
mov cl, 7
myjmp TWINE_50_0
TWINE_39_12 equ $
jz ok37
myjmp TWINE_39_11
TWINE_49_0 equ $
ret
TWINE_50_0 equ $
ret
TWINE_39_11 equ $
fallthrough37:xor al, 58^255
myjmp TWINE_39_10
TWINE_39_10 equ $
inc al
myjmp TWINE_39_9
TWINE_51_38 equ $
twine18_start: add al, 256-110
myjmp TWINE_51_37
TWINE_52_2 equ $
ok75: mov al, 67
myjmp TWINE_52_1
TWINE_39_9 equ $
jz ok58
myjmp TWINE_39_8
TWINE_51_37 equ $
jz ok110
 add al, 110
myjmp TWINE_51_36
TWINE_52_1 equ $
mov cl, 7
myjmp TWINE_52_0
TWINE_39_8 equ $
dec al
myjmp TWINE_39_7
TWINE_51_36 equ $
fallthrough110:xor rcx, rcx
 xchg rax, rdx
myjmp TWINE_51_35
TWINE_52_0 equ $
ret
TWINE_39_7 equ $
xor al, 58^255
myjmp TWINE_39_6
TWINE_51_35 equ $
mov rax, 3416409459
myjmp TWINE_51_34
TWINE_39_6 equ $
fallthrough58:xor rcx, rcx
 mov cl, 123
myjmp TWINE_39_5
TWINE_51_34 equ $
xchg rax, rdx
myjmp TWINE_51_33
TWINE_53_78 equ $
twine9_start: xor ah, ah
myjmp TWINE_53_77
TWINE_39_5 equ $
lp123: dec al
myjmp TWINE_39_4
TWINE_51_33 equ $
crc32 rcx, al
myjmp TWINE_51_32
TWINE_53_77 equ $
mov r8w, ax
myjmp TWINE_53_76
TWINE_39_4 equ $
loop lp123
myjmp TWINE_39_3
TWINE_51_32 equ $
cmp rcx, rdx
 jz ok72
myjmp TWINE_51_31
TWINE_53_76 equ $

myjmp TWINE_53_75
TWINE_39_3 equ $
test al, al
myjmp TWINE_39_2
TWINE_51_31 equ $
fallthrough72:xor ah, ah
myjmp TWINE_51_30
TWINE_53_75 equ $
bsf cx, r8w
myjmp TWINE_53_74
TWINE_39_2 equ $
jz ok123
myjmp TWINE_39_1
TWINE_51_30 equ $
mov r8w, ax
myjmp TWINE_51_29
TWINE_53_74 equ $
shr r8w, 1+1
myjmp TWINE_53_73
TWINE_39_1 equ $
add al, 123
myjmp TWINE_39_0
TWINE_51_29 equ $

myjmp TWINE_51_28
TWINE_53_73 equ $
cmp cx, 1
myjmp TWINE_53_72
TWINE_39_0 equ $
fallthrough123:jmp twine7_start
TWINE_51_28 equ $
bsf cx, r8w
myjmp TWINE_51_27
TWINE_53_72 equ $
jnz fallthrough122
myjmp TWINE_53_71
TWINE_51_27 equ $
shr r8w, 1+1
myjmp TWINE_51_26
TWINE_53_71 equ $

myjmp TWINE_53_70
TWINE_54_2 equ $
ok72: mov al, 95
myjmp TWINE_54_1
TWINE_51_26 equ $
cmp cx, 1
myjmp TWINE_51_25
TWINE_53_70 equ $
bsf cx, r8w
myjmp TWINE_53_69
TWINE_54_1 equ $
mov cl, 7
myjmp TWINE_54_0
TWINE_51_25 equ $
jnz fallthrough46
myjmp TWINE_51_24
TWINE_53_69 equ $
shr r8w, 1+1
myjmp TWINE_53_68
TWINE_54_0 equ $
ret
TWINE_51_24 equ $

myjmp TWINE_51_23
TWINE_53_68 equ $
cmp cx, 1
myjmp TWINE_53_67
TWINE_51_23 equ $
bsf cx, r8w
myjmp TWINE_51_22
TWINE_53_67 equ $
jnz fallthrough122
myjmp TWINE_53_66
TWINE_55_2 equ $
ok69: mov al, 15
myjmp TWINE_55_1
TWINE_51_22 equ $
shr r8w, 0+1
myjmp TWINE_51_21
TWINE_53_66 equ $

myjmp TWINE_53_65
TWINE_55_1 equ $
mov cl, 6
myjmp TWINE_55_0
TWINE_51_21 equ $
cmp cx, 0
myjmp TWINE_51_20
TWINE_53_65 equ $
bsf cx, r8w
myjmp TWINE_53_64
TWINE_55_0 equ $
ret
TWINE_51_20 equ $
jnz fallthrough46
myjmp TWINE_51_19
TWINE_53_64 equ $
shr r8w, 0+1
myjmp TWINE_53_63
TWINE_51_19 equ $

myjmp TWINE_51_18
TWINE_53_63 equ $
cmp cx, 0
myjmp TWINE_53_62
TWINE_56_2 equ $
ok13: mov al, 62
myjmp TWINE_56_1
TWINE_51_18 equ $
bsf cx, r8w
myjmp TWINE_51_17
TWINE_53_62 equ $
jnz fallthrough122
myjmp TWINE_53_61
TWINE_56_1 equ $
mov cl, 7
myjmp TWINE_56_0
TWINE_51_17 equ $
shr r8w, 0+1
myjmp TWINE_51_16
TWINE_53_61 equ $

myjmp TWINE_53_60
TWINE_56_0 equ $
ret
TWINE_51_16 equ $
cmp cx, 0
myjmp TWINE_51_15
TWINE_53_60 equ $
bsf cx, r8w
myjmp TWINE_53_59
TWINE_51_15 equ $
jnz fallthrough46
myjmp TWINE_51_14
TWINE_53_59 equ $
shr r8w, 0+1
myjmp TWINE_53_58
TWINE_57_27 equ $
twine5_start: sub al, 50
myjmp TWINE_57_26
TWINE_51_14 equ $

myjmp TWINE_51_13
TWINE_53_58 equ $
cmp cx, 0
myjmp TWINE_53_57
TWINE_57_26 equ $
test al, al
myjmp TWINE_57_25
TWINE_51_13 equ $
bsf cx, r8w
myjmp TWINE_51_12
TWINE_53_57 equ $
jnz fallthrough122
myjmp TWINE_53_56
TWINE_57_25 equ $
jz ok50
myjmp TWINE_57_24
TWINE_51_12 equ $
shr r8w, 1+1
myjmp TWINE_51_11
TWINE_53_56 equ $

myjmp TWINE_53_55
TWINE_57_24 equ $
add al, 50
myjmp TWINE_57_23
TWINE_51_11 equ $
cmp cx, 1
myjmp TWINE_51_10
TWINE_53_55 equ $
bsf cx, r8w
myjmp TWINE_53_54
TWINE_57_23 equ $
fallthrough50:xor al, 104
myjmp TWINE_57_22
TWINE_51_10 equ $
jnz fallthrough46
myjmp TWINE_51_9
TWINE_53_54 equ $
shr r8w, 0+1
myjmp TWINE_53_53
TWINE_57_22 equ $
jz ok104
 xor al, 104
myjmp TWINE_57_21
TWINE_51_9 equ $
test r8w, r8w
myjmp TWINE_51_8
TWINE_53_53 equ $
cmp cx, 0
myjmp TWINE_53_52
TWINE_57_21 equ $
fallthrough104:xor al, 115^255
myjmp TWINE_57_20
TWINE_51_8 equ $
jz ok46
myjmp TWINE_51_7
TWINE_53_52 equ $
jnz fallthrough122
myjmp TWINE_53_51
TWINE_57_20 equ $
inc al
myjmp TWINE_57_19
TWINE_51_7 equ $
fallthrough46:test al, 255^91
myjmp TWINE_51_6
TWINE_53_51 equ $
test r8w, r8w
myjmp TWINE_53_50
TWINE_57_19 equ $
jz ok115
myjmp TWINE_57_18
TWINE_51_6 equ $
jnz fallthrough91
myjmp TWINE_51_5
TWINE_53_50 equ $
jz ok122
myjmp TWINE_53_49
TWINE_57_18 equ $
dec al
myjmp TWINE_57_17
TWINE_51_5 equ $
mov r8b, al
myjmp TWINE_51_4
TWINE_53_49 equ $
fallthrough122:xor rcx, rcx
 mov cl, 71
myjmp TWINE_53_48
TWINE_57_17 equ $
xor al, 115^255
myjmp TWINE_57_16
TWINE_51_4 equ $
or al, 255^91
myjmp TWINE_51_3
TWINE_53_48 equ $
lp71: dec al
myjmp TWINE_53_47
TWINE_57_16 equ $
fallthrough115:add al, 256-69
myjmp TWINE_57_15
TWINE_51_3 equ $
cmp al, 255
myjmp TWINE_51_2
TWINE_53_47 equ $
loop lp71
myjmp TWINE_53_46
TWINE_57_15 equ $
jz ok69
 add al, 69
myjmp TWINE_57_14
TWINE_51_2 equ $
jz ok91
myjmp TWINE_51_1
TWINE_53_46 equ $
test al, al
myjmp TWINE_53_45
TWINE_57_14 equ $
fallthrough69:mov cl, 255^79
myjmp TWINE_57_13
TWINE_51_1 equ $
mov al, r8b
myjmp TWINE_51_0
TWINE_53_45 equ $
jz ok71
myjmp TWINE_53_44
TWINE_57_13 equ $
xor cl, al
myjmp TWINE_57_12
TWINE_51_0 equ $
fallthrough91:jmp ohno
TWINE_53_44 equ $
add al, 71
myjmp TWINE_53_43
TWINE_57_12 equ $
inc cl
myjmp TWINE_57_11
TWINE_53_43 equ $
fallthrough71:add al, 256-75
myjmp TWINE_53_42
TWINE_57_11 equ $
test cl, cl
myjmp TWINE_57_10
TWINE_58_2 equ $
ok73: mov al, 99
myjmp TWINE_58_1
TWINE_53_42 equ $
jz ok75
 add al, 75
myjmp TWINE_53_41
TWINE_57_10 equ $
jz ok79
myjmp TWINE_57_9
TWINE_58_1 equ $
mov cl, 7
myjmp TWINE_58_0
TWINE_53_41 equ $
fallthrough75:sub al, 77
myjmp TWINE_53_40
TWINE_57_9 equ $
fallthrough79:mov cl, 255^83
myjmp TWINE_57_8
TWINE_58_0 equ $
ret
TWINE_53_40 equ $
test al, al
myjmp TWINE_53_39
TWINE_57_8 equ $
xor cl, al
myjmp TWINE_57_7
TWINE_53_39 equ $
jz ok77
myjmp TWINE_53_38
TWINE_57_7 equ $
inc cl
myjmp TWINE_57_6
TWINE_59_27 equ $
twine17_start: xor al, 53^255
myjmp TWINE_59_26
TWINE_53_38 equ $
add al, 77
myjmp TWINE_53_37
TWINE_57_6 equ $
test cl, cl
myjmp TWINE_57_5
TWINE_59_26 equ $
inc al
myjmp TWINE_59_25
TWINE_53_37 equ $
fallthrough77:mov cl, 255^85
myjmp TWINE_53_36
TWINE_57_5 equ $
jz ok83
myjmp TWINE_57_4
TWINE_59_25 equ $
jz ok53
myjmp TWINE_59_24
TWINE_53_36 equ $
xor cl, al
myjmp TWINE_53_35
TWINE_57_4 equ $
fallthrough83:rol al, 3
myjmp TWINE_57_3
TWINE_59_24 equ $
dec al
myjmp TWINE_59_23
TWINE_53_35 equ $
inc cl
myjmp TWINE_53_34
TWINE_57_3 equ $
cmp al, ((92<<3) | (92>>5)) & 255
myjmp TWINE_57_2
TWINE_59_23 equ $
xor al, 53^255
myjmp TWINE_59_22
TWINE_53_34 equ $
test cl, cl
myjmp TWINE_53_33
TWINE_57_2 equ $
jz ok92
myjmp TWINE_57_1
TWINE_59_22 equ $
fallthrough53:rol al, 3
myjmp TWINE_59_21
TWINE_53_33 equ $
jz ok85
myjmp TWINE_53_32
TWINE_57_1 equ $
ror al, 3
myjmp TWINE_57_0
TWINE_59_21 equ $
cmp al, ((73<<3) | (73>>5)) & 255
myjmp TWINE_59_20
TWINE_53_32 equ $
fallthrough85:bt ax, 0
myjmp TWINE_53_31
TWINE_57_0 equ $
fallthrough92:jmp twine6_start
TWINE_59_20 equ $
jz ok73
myjmp TWINE_59_19
TWINE_53_31 equ $
jc fallthrough38
myjmp TWINE_53_30
TWINE_59_19 equ $
ror al, 3
myjmp TWINE_59_18
TWINE_53_30 equ $
bt ax, 1
myjmp TWINE_53_29
TWINE_60_2 equ $
ok118: mov al, 18
myjmp TWINE_60_1
TWINE_59_18 equ $
fallthrough73:xor rcx, rcx
 xchg rax, rdx
myjmp TWINE_59_17
TWINE_53_29 equ $
jnc fallthrough38
myjmp TWINE_53_28
TWINE_60_1 equ $
mov cl, 7
myjmp TWINE_60_0
TWINE_59_17 equ $
mov rax, 2739821008
myjmp TWINE_59_16
TWINE_53_28 equ $
bt ax, 2
myjmp TWINE_53_27
TWINE_60_0 equ $
ret
TWINE_59_16 equ $
xchg rax, rdx
myjmp TWINE_59_15
TWINE_53_27 equ $
jnc fallthrough38
myjmp TWINE_53_26
TWINE_59_15 equ $
crc32 rcx, al
myjmp TWINE_59_14
TWINE_53_26 equ $
bt ax, 3
myjmp TWINE_53_25
TWINE_61_16 equ $
twine12_start: rol al, 3
myjmp TWINE_61_15
TWINE_59_14 equ $
cmp rcx, rdx
 jz ok81
myjmp TWINE_59_13
TWINE_53_25 equ $
jc fallthrough38
myjmp TWINE_53_24
TWINE_61_15 equ $
cmp al, ((98<<3) | (98>>5)) & 255
myjmp TWINE_61_14
TWINE_59_13 equ $
fallthrough81:mov cl, 14
myjmp TWINE_59_12
TWINE_53_24 equ $
bt ax, 4
myjmp TWINE_53_23
TWINE_61_14 equ $
jz ok98
myjmp TWINE_61_13
TWINE_59_12 equ $
mov r8b, al
myjmp TWINE_59_11
TWINE_53_23 equ $
jc fallthrough38
myjmp TWINE_53_22
TWINE_61_13 equ $
ror al, 3
myjmp TWINE_61_12
TWINE_59_11 equ $
mul cl
myjmp TWINE_59_10
TWINE_53_22 equ $
bt ax, 5
myjmp TWINE_53_21
TWINE_61_12 equ $
fallthrough98:test al, 255^119
myjmp TWINE_61_11
TWINE_59_10 equ $
cmp ax, 14*44
myjmp TWINE_59_9
TWINE_53_21 equ $
jnc fallthrough38
myjmp TWINE_53_20
TWINE_61_11 equ $
jnz fallthrough119
myjmp TWINE_61_10
TWINE_59_9 equ $
jz ok44
myjmp TWINE_59_8
TWINE_53_20 equ $
bt ax, 6
myjmp TWINE_53_19
TWINE_61_10 equ $
mov r8b, al
myjmp TWINE_61_9
TWINE_59_8 equ $
mov al, r8b
myjmp TWINE_59_7
TWINE_53_19 equ $
jc fallthrough38
myjmp TWINE_53_18
TWINE_61_9 equ $
or al, 255^119
myjmp TWINE_61_8
TWINE_59_7 equ $
fallthrough44:test al, 255^61
myjmp TWINE_59_6
TWINE_53_18 equ $
jmp ok38
myjmp TWINE_53_17
TWINE_61_8 equ $
cmp al, 255
myjmp TWINE_61_7
TWINE_59_6 equ $
jnz fallthrough61
myjmp TWINE_59_5
TWINE_53_17 equ $
fallthrough38:xor al, 42^255
myjmp TWINE_53_16
TWINE_61_7 equ $
jz ok119
myjmp TWINE_61_6
TWINE_59_5 equ $
mov r8b, al
myjmp TWINE_59_4
TWINE_53_16 equ $
inc al
myjmp TWINE_53_15
TWINE_61_6 equ $
mov al, r8b
myjmp TWINE_61_5
TWINE_59_4 equ $
or al, 255^61
myjmp TWINE_59_3
TWINE_53_15 equ $
jz ok42
myjmp TWINE_53_14
TWINE_61_5 equ $
fallthrough119:xor rcx, rcx
 xchg rax, rdx
myjmp TWINE_61_4
TWINE_59_3 equ $
cmp al, 255
myjmp TWINE_59_2
TWINE_53_14 equ $
dec al
myjmp TWINE_53_13
TWINE_61_4 equ $
mov rax, 3976438427
myjmp TWINE_61_3
TWINE_59_2 equ $
jz ok61
myjmp TWINE_59_1
TWINE_53_13 equ $
xor al, 42^255
myjmp TWINE_53_12
TWINE_61_3 equ $
xchg rax, rdx
myjmp TWINE_61_2
TWINE_59_1 equ $
mov al, r8b
myjmp TWINE_59_0
TWINE_53_12 equ $
fallthrough42:mov cl, 255^64
myjmp TWINE_53_11
TWINE_61_2 equ $
crc32 rcx, al
myjmp TWINE_61_1
TWINE_59_0 equ $
fallthrough61:jmp twine18_start
TWINE_53_11 equ $
xor cl, al
myjmp TWINE_53_10
TWINE_61_1 equ $
cmp rcx, rdx
 jz ok78
myjmp TWINE_61_0
TWINE_53_10 equ $
inc cl
myjmp TWINE_53_9
TWINE_61_0 equ $
fallthrough78:jmp twine13_start
TWINE_62_45 equ $
twine11_start: sub al, 51
myjmp TWINE_62_44
TWINE_53_9 equ $
test cl, cl
myjmp TWINE_53_8
TWINE_62_44 equ $
test al, al
myjmp TWINE_62_43
TWINE_53_8 equ $
jz ok64
myjmp TWINE_53_7
TWINE_63_2 equ $
ok103: mov al, 93
myjmp TWINE_63_1
TWINE_62_43 equ $
jz ok51
myjmp TWINE_62_42
TWINE_53_7 equ $
fallthrough64:test al, 255^94
myjmp TWINE_53_6
TWINE_63_1 equ $
mov cl, 7
myjmp TWINE_63_0
TWINE_62_42 equ $
add al, 51
myjmp TWINE_62_41
TWINE_53_6 equ $
jnz fallthrough94
myjmp TWINE_53_5
TWINE_63_0 equ $
ret
TWINE_62_41 equ $
fallthrough51:mov cl, 211
myjmp TWINE_62_40
TWINE_53_5 equ $
mov r8b, al
myjmp TWINE_53_4
TWINE_62_40 equ $
mov r8b, al
myjmp TWINE_62_39
TWINE_53_4 equ $
or al, 255^94
myjmp TWINE_53_3
TWINE_64_2 equ $
ok42: mov al, 58
myjmp TWINE_64_1
TWINE_62_39 equ $
mul cl
myjmp TWINE_62_38
TWINE_53_3 equ $
cmp al, 255
myjmp TWINE_53_2
TWINE_64_1 equ $
mov cl, 7
myjmp TWINE_64_0
TWINE_62_38 equ $
cmp ax, 211*114
myjmp TWINE_62_37
TWINE_53_2 equ $
jz ok94
myjmp TWINE_53_1
TWINE_64_0 equ $
ret
TWINE_62_37 equ $
jz ok114
myjmp TWINE_62_36
TWINE_53_1 equ $
mov al, r8b
myjmp TWINE_53_0
TWINE_62_36 equ $
mov al, r8b
myjmp TWINE_62_35
TWINE_53_0 equ $
fallthrough94:jmp twine10_start
TWINE_65_2 equ $
ok114: mov al, 32
myjmp TWINE_65_1
TWINE_62_35 equ $
fallthrough114:xor ah, ah
myjmp TWINE_62_34
TWINE_65_1 equ $
mov cl, 6
myjmp TWINE_65_0
TWINE_62_34 equ $
mov r8w, ax
myjmp TWINE_62_33
TWINE_66_2 equ $
ok34: mov al, 106
myjmp TWINE_66_1
TWINE_65_0 equ $
ret
TWINE_62_33 equ $

myjmp TWINE_62_32
TWINE_66_1 equ $
mov cl, 7
myjmp TWINE_66_0
TWINE_62_32 equ $
bsf cx, r8w
myjmp TWINE_62_31
TWINE_66_0 equ $
ret
TWINE_67_2 equ $
ok116: mov al, 28
myjmp TWINE_67_1
TWINE_62_31 equ $
shr r8w, 1+1
myjmp TWINE_62_30
TWINE_67_1 equ $
mov cl, 6
myjmp TWINE_67_0
TWINE_62_30 equ $
cmp cx, 1
myjmp TWINE_62_29
TWINE_68_2 equ $
ok125: mov al, 12
myjmp TWINE_68_1
TWINE_67_0 equ $
ret
TWINE_62_29 equ $
jnz fallthrough66
myjmp TWINE_62_28
TWINE_68_1 equ $
mov cl, 6
myjmp TWINE_68_0
TWINE_62_28 equ $

myjmp TWINE_62_27
TWINE_68_0 equ $
ret
TWINE_69_2 equ $
ok76: mov al, 37
myjmp TWINE_69_1
TWINE_62_27 equ $
bsf cx, r8w
myjmp TWINE_62_26
TWINE_69_1 equ $
mov cl, 7
myjmp TWINE_69_0
TWINE_62_26 equ $
shr r8w, 4+1
myjmp TWINE_62_25
TWINE_70_21 equ $
twine4_start: xor al, 54^255
myjmp TWINE_70_20
TWINE_69_0 equ $
ret
TWINE_62_25 equ $
cmp cx, 4
myjmp TWINE_62_24
TWINE_70_20 equ $
inc al
myjmp TWINE_70_19
TWINE_62_24 equ $
jnz fallthrough66
myjmp TWINE_62_23
TWINE_70_19 equ $
jz ok54
myjmp TWINE_70_18
TWINE_71_27 equ $
twine15_start: xor rcx, rcx
 mov cl, 56
myjmp TWINE_71_26
TWINE_62_23 equ $
test r8w, r8w
myjmp TWINE_62_22
TWINE_70_18 equ $
dec al
myjmp TWINE_70_17
TWINE_71_26 equ $
lp56: dec al
myjmp TWINE_71_25
TWINE_62_22 equ $
jz ok66
myjmp TWINE_62_21
TWINE_70_17 equ $
xor al, 54^255
myjmp TWINE_70_16
TWINE_71_25 equ $
loop lp56
myjmp TWINE_71_24
TWINE_62_21 equ $
fallthrough66:bt ax, 0
myjmp TWINE_62_20
TWINE_70_16 equ $
fallthrough54:xor rcx, rcx
 xchg rax, rdx
myjmp TWINE_70_15
TWINE_71_24 equ $
test al, al
myjmp TWINE_71_23
TWINE_62_20 equ $
jc fallthrough40
myjmp TWINE_62_19
TWINE_70_15 equ $
mov rax, 2477592673
myjmp TWINE_70_14
TWINE_71_23 equ $
jz ok56
myjmp TWINE_71_22
TWINE_62_19 equ $
bt ax, 1
myjmp TWINE_62_18
TWINE_70_14 equ $
xchg rax, rdx
myjmp TWINE_70_13
TWINE_71_22 equ $
add al, 56
myjmp TWINE_71_21
TWINE_62_18 equ $
jc fallthrough40
myjmp TWINE_62_17
TWINE_70_13 equ $
crc32 rcx, al
myjmp TWINE_70_12
TWINE_71_21 equ $
fallthrough56:xor rcx, rcx
 mov cl, 68
myjmp TWINE_71_20
TWINE_62_17 equ $
bt ax, 2
myjmp TWINE_62_16
TWINE_70_12 equ $
cmp rcx, rdx
 jz ok97
myjmp TWINE_70_11
TWINE_71_20 equ $
lp68: dec al
myjmp TWINE_71_19
TWINE_62_16 equ $
jc fallthrough40
myjmp TWINE_62_15
TWINE_70_11 equ $
fallthrough97:mov cl, 255^101
myjmp TWINE_70_10
TWINE_71_19 equ $
loop lp68
myjmp TWINE_71_18
TWINE_62_15 equ $
bt ax, 3
myjmp TWINE_62_14
TWINE_70_10 equ $
xor cl, al
myjmp TWINE_70_9
TWINE_71_18 equ $
test al, al
myjmp TWINE_71_17
TWINE_62_14 equ $
jnc fallthrough40
myjmp TWINE_62_13
TWINE_70_9 equ $
inc cl
myjmp TWINE_70_8
TWINE_71_17 equ $
jz ok68
myjmp TWINE_71_16
TWINE_62_13 equ $
bt ax, 4
myjmp TWINE_62_12
TWINE_70_8 equ $
test cl, cl
myjmp TWINE_70_7
TWINE_71_16 equ $
add al, 68
myjmp TWINE_71_15
TWINE_62_12 equ $
jc fallthrough40
myjmp TWINE_62_11
TWINE_70_7 equ $
jz ok101
myjmp TWINE_70_6
TWINE_71_15 equ $
fallthrough68:bt ax, 0
myjmp TWINE_71_14
TWINE_62_11 equ $
bt ax, 5
myjmp TWINE_62_10
TWINE_70_6 equ $
fallthrough101:xor rcx, rcx
 mov cl, 80
myjmp TWINE_70_5
TWINE_71_14 equ $
jnc fallthrough125
myjmp TWINE_71_13
TWINE_62_10 equ $
jnc fallthrough40
myjmp TWINE_62_9
TWINE_70_5 equ $
lp80: dec al
myjmp TWINE_70_4
TWINE_71_13 equ $
bt ax, 1
myjmp TWINE_71_12
TWINE_62_9 equ $
bt ax, 6
myjmp TWINE_62_8
TWINE_70_4 equ $
loop lp80
myjmp TWINE_70_3
TWINE_71_12 equ $
jc fallthrough125
myjmp TWINE_71_11
TWINE_62_8 equ $
jc fallthrough40
myjmp TWINE_62_7
TWINE_70_3 equ $
test al, al
myjmp TWINE_70_2
TWINE_71_11 equ $
bt ax, 2
myjmp TWINE_71_10
TWINE_62_7 equ $
jmp ok40
myjmp TWINE_62_6
TWINE_70_2 equ $
jz ok80
myjmp TWINE_70_1
TWINE_71_10 equ $
jnc fallthrough125
myjmp TWINE_71_9
TWINE_62_6 equ $
fallthrough40:mov cl, 88
myjmp TWINE_62_5
TWINE_70_1 equ $
add al, 80
myjmp TWINE_70_0
TWINE_71_9 equ $
bt ax, 3
myjmp TWINE_71_8
TWINE_62_5 equ $
mov r8b, al
myjmp TWINE_62_4
TWINE_70_0 equ $
fallthrough80:jmp twine5_start
TWINE_71_8 equ $
jnc fallthrough125
myjmp TWINE_71_7
TWINE_62_4 equ $
mul cl
myjmp TWINE_62_3
TWINE_71_7 equ $
bt ax, 4
myjmp TWINE_71_6
TWINE_62_3 equ $
cmp ax, 88*96
myjmp TWINE_62_2
TWINE_72_2 equ $
ok121: mov al, 23
myjmp TWINE_72_1
TWINE_71_6 equ $
jnc fallthrough125
myjmp TWINE_71_5
TWINE_62_2 equ $
jz ok96
myjmp TWINE_62_1
TWINE_72_1 equ $
mov cl, 6
myjmp TWINE_72_0
TWINE_71_5 equ $
bt ax, 5
myjmp TWINE_71_4
TWINE_62_1 equ $
mov al, r8b
myjmp TWINE_62_0
TWINE_72_0 equ $
ret
TWINE_71_4 equ $
jnc fallthrough125
myjmp TWINE_71_3
TWINE_62_0 equ $
fallthrough96:jmp twine12_start
TWINE_71_3 equ $
bt ax, 6
myjmp TWINE_71_2
TWINE_73_2 equ $
ok48: mov al, 36
myjmp TWINE_73_1
TWINE_71_2 equ $
jnc fallthrough125
myjmp TWINE_71_1
TWINE_74_2 equ $
ok105: mov al, 51
myjmp TWINE_74_1
TWINE_73_1 equ $
mov cl, 6
myjmp TWINE_73_0
TWINE_71_1 equ $
jmp ok125
myjmp TWINE_71_0
TWINE_74_1 equ $
mov cl, 7
myjmp TWINE_74_0
TWINE_73_0 equ $
ret
TWINE_71_0 equ $
fallthrough125:jmp twine16_start
TWINE_74_0 equ $
ret
TWINE_75_2 equ $
ok102: mov al, 73
myjmp TWINE_75_1
TWINE_76_2 equ $
ok113: mov al, 45
myjmp TWINE_76_1
TWINE_77_2 equ $
ok81: mov al, 122
myjmp TWINE_77_1
TWINE_75_1 equ $
mov cl, 7
myjmp TWINE_75_0
TWINE_76_1 equ $
mov cl, 7
myjmp TWINE_76_0
TWINE_77_1 equ $
mov cl, 7
myjmp TWINE_77_0
TWINE_75_0 equ $
ret
TWINE_76_0 equ $
ret
TWINE_77_0 equ $
ret
TWINE_78_2 equ $
ok37: mov al, 29
myjmp TWINE_78_1
TWINE_79_2 equ $
ok55: mov al, 48
myjmp TWINE_79_1
TWINE_80_60 equ $
twine16_start: test al, 255^48
myjmp TWINE_80_59
TWINE_78_1 equ $
mov cl, 7
myjmp TWINE_78_0
TWINE_79_1 equ $
mov cl, 6
myjmp TWINE_79_0
TWINE_80_59 equ $
jnz fallthrough48
myjmp TWINE_80_58
TWINE_78_0 equ $
ret
TWINE_79_0 equ $
ret
TWINE_80_58 equ $
mov r8b, al
myjmp TWINE_80_57
TWINE_80_57 equ $
or al, 255^48
myjmp TWINE_80_56
TWINE_81_2 equ $
ok115: mov al, 40
myjmp TWINE_81_1
TWINE_82_2 equ $
ok68: mov al, 127
myjmp TWINE_82_1
TWINE_80_56 equ $
cmp al, 255
myjmp TWINE_80_55
TWINE_81_1 equ $
mov cl, 6
myjmp TWINE_81_0
TWINE_82_1 equ $
mov cl, 7
myjmp TWINE_82_0
TWINE_80_55 equ $
jz ok48
myjmp TWINE_80_54
TWINE_81_0 equ $
ret
TWINE_82_0 equ $
ret
TWINE_80_54 equ $
mov al, r8b
myjmp TWINE_80_53
TWINE_80_53 equ $
fallthrough48:test al, 255^100
myjmp TWINE_80_52
TWINE_83_2 equ $
ok83: mov al, 43
myjmp TWINE_83_1
TWINE_84_2 equ $
ok57: mov al, 33
myjmp TWINE_84_1
TWINE_80_52 equ $
jnz fallthrough100
myjmp TWINE_80_51
TWINE_83_1 equ $
mov cl, 6
myjmp TWINE_83_0
TWINE_84_1 equ $
mov cl, 7
myjmp TWINE_84_0
TWINE_80_51 equ $
mov r8b, al
myjmp TWINE_80_50
TWINE_83_0 equ $
ret
TWINE_84_0 equ $
ret
TWINE_80_50 equ $
or al, 255^100
myjmp TWINE_80_49
TWINE_80_49 equ $
cmp al, 255
myjmp TWINE_80_48
TWINE_85_2 equ $
ok119: mov al, 115
myjmp TWINE_85_1
TWINE_86_2 equ $
ok122: mov al, 54
myjmp TWINE_86_1
TWINE_80_48 equ $
jz ok100
myjmp TWINE_80_47
TWINE_85_1 equ $
mov cl, 7
myjmp TWINE_85_0
TWINE_86_1 equ $
mov cl, 7
myjmp TWINE_86_0
TWINE_80_47 equ $
mov al, r8b
myjmp TWINE_80_46
TWINE_85_0 equ $
ret
TWINE_86_0 equ $
ret
TWINE_80_46 equ $
fallthrough100:bt ax, 0
myjmp TWINE_80_45
TWINE_80_45 equ $
jc fallthrough118
myjmp TWINE_80_44
TWINE_87_22 equ $
twine2_start: cmp al, 99
myjmp TWINE_87_21
TWINE_88_15 equ $
twine10_start: test al, 255^107
myjmp TWINE_88_14
TWINE_80_44 equ $
bt ax, 1
myjmp TWINE_80_43
TWINE_87_21 equ $
jz ok99
myjmp TWINE_87_20
TWINE_88_14 equ $
jnz fallthrough107
myjmp TWINE_88_13
TWINE_80_43 equ $
jnc fallthrough118
myjmp TWINE_80_42
TWINE_87_20 equ $
fallthrough99:xor rcx, rcx
 xchg rax, rdx
myjmp TWINE_87_19
TWINE_88_13 equ $
mov r8b, al
myjmp TWINE_88_12
TWINE_80_42 equ $
bt ax, 2
myjmp TWINE_80_41
TWINE_87_19 equ $
mov rax, 1197962378
myjmp TWINE_87_18
TWINE_88_12 equ $
or al, 255^107
myjmp TWINE_88_11
TWINE_80_41 equ $
jnc fallthrough118
myjmp TWINE_80_40
TWINE_87_18 equ $
xchg rax, rdx
myjmp TWINE_87_17
TWINE_88_11 equ $
cmp al, 255
myjmp TWINE_88_10
TWINE_80_40 equ $
bt ax, 3
myjmp TWINE_80_39
TWINE_87_17 equ $
crc32 rcx, al
myjmp TWINE_87_16
TWINE_88_10 equ $
jz ok107
myjmp TWINE_88_9
TWINE_80_39 equ $
jc fallthrough118
myjmp TWINE_80_38
TWINE_87_16 equ $
cmp rcx, rdx
 jz ok102
myjmp TWINE_87_15
TWINE_88_9 equ $
mov al, r8b
myjmp TWINE_88_8
TWINE_80_38 equ $
bt ax, 4
myjmp TWINE_80_37
TWINE_87_15 equ $
fallthrough102:sub al, 109
myjmp TWINE_87_14
TWINE_88_8 equ $
fallthrough107:mov cl, 243
myjmp TWINE_88_7
TWINE_80_37 equ $
jnc fallthrough118
myjmp TWINE_80_36
TWINE_87_14 equ $
test al, al
myjmp TWINE_87_13
TWINE_88_7 equ $
mov r8b, al
myjmp TWINE_88_6
TWINE_80_36 equ $
bt ax, 5
myjmp TWINE_80_35
TWINE_87_13 equ $
jz ok109
myjmp TWINE_87_12
TWINE_88_6 equ $
mul cl
myjmp TWINE_88_5
TWINE_80_35 equ $
jnc fallthrough118
myjmp TWINE_80_34
TWINE_87_12 equ $
add al, 109
myjmp TWINE_87_11
TWINE_88_5 equ $
cmp ax, 243*63
myjmp TWINE_88_4
TWINE_80_34 equ $
bt ax, 6
myjmp TWINE_80_33
TWINE_87_11 equ $
fallthrough109:xor rcx, rcx
 xchg rax, rdx
myjmp TWINE_87_10
TWINE_88_4 equ $
jz ok63
myjmp TWINE_88_3
TWINE_80_33 equ $
jnc fallthrough118
myjmp TWINE_80_32
TWINE_87_10 equ $
mov rax, 3690758684
myjmp TWINE_87_9
TWINE_88_3 equ $
mov al, r8b
myjmp TWINE_88_2
TWINE_80_32 equ $
jmp ok118
myjmp TWINE_80_31
TWINE_87_9 equ $
xchg rax, rdx
myjmp TWINE_87_8
TWINE_88_2 equ $
fallthrough63:xor al, 10
myjmp TWINE_88_1
TWINE_80_31 equ $
fallthrough118:test al, 255^67
myjmp TWINE_80_30
TWINE_87_8 equ $
crc32 rcx, al
myjmp TWINE_87_7
TWINE_88_1 equ $
jz ok10
 xor al, 10
myjmp TWINE_88_0
TWINE_80_30 equ $
jnz fallthrough67
myjmp TWINE_80_29
TWINE_87_7 equ $
cmp rcx, rdx
 jz ok88
myjmp TWINE_87_6
TWINE_88_0 equ $
fallthrough10:jmp twine11_start
TWINE_80_29 equ $
mov r8b, al
myjmp TWINE_80_28
TWINE_87_6 equ $
fallthrough88:mov cl, 114
myjmp TWINE_87_5
TWINE_80_28 equ $
or al, 255^67
myjmp TWINE_80_27
TWINE_87_5 equ $
mov r8b, al
myjmp TWINE_87_4
TWINE_89_2 equ $
ok109: mov al, 44
myjmp TWINE_89_1
TWINE_80_27 equ $
cmp al, 255
myjmp TWINE_80_26
TWINE_87_4 equ $
mul cl
myjmp TWINE_87_3
TWINE_89_1 equ $
mov cl, 6
myjmp TWINE_89_0
TWINE_80_26 equ $
jz ok67
myjmp TWINE_80_25
TWINE_87_3 equ $
cmp ax, 114*36
myjmp TWINE_87_2
TWINE_89_0 equ $
ret
TWINE_80_25 equ $
mov al, r8b
myjmp TWINE_80_24
TWINE_87_2 equ $
jz ok36
myjmp TWINE_87_1
TWINE_80_24 equ $
fallthrough67:sub al, 76
myjmp TWINE_80_23
TWINE_87_1 equ $
mov al, r8b
myjmp TWINE_87_0
TWINE_90_2 equ $
ok92: mov al, 50
myjmp TWINE_90_1
TWINE_80_23 equ $
test al, al
myjmp TWINE_80_22
TWINE_87_0 equ $
fallthrough36:jmp twine3_start
TWINE_90_1 equ $
mov cl, 7
myjmp TWINE_90_0
TWINE_80_22 equ $
jz ok76
myjmp TWINE_80_21
TWINE_90_0 equ $
ret
TWINE_80_21 equ $
add al, 76
myjmp TWINE_80_20
TWINE_91_2 equ $
ok66: mov al, 101
myjmp TWINE_91_1
TWINE_80_20 equ $
fallthrough76:sub al, 84
myjmp TWINE_80_19
TWINE_91_1 equ $
mov cl, 7
myjmp TWINE_91_0
TWINE_92_2 equ $
ok100: mov al, 49
myjmp TWINE_92_1
TWINE_80_19 equ $
test al, al
myjmp TWINE_80_18
TWINE_91_0 equ $
ret
TWINE_92_1 equ $
mov cl, 7
myjmp TWINE_92_0
TWINE_80_18 equ $
jz ok84
myjmp TWINE_80_17
TWINE_92_0 equ $
ret
TWINE_80_17 equ $
add al, 84
myjmp TWINE_80_16
TWINE_93_2 equ $
ok12: mov al, 110
myjmp TWINE_93_1
TWINE_80_16 equ $
fallthrough84:sub ah, ah
 sub ax, 39
myjmp TWINE_80_15
TWINE_93_1 equ $
mov cl, 7
myjmp TWINE_93_0
TWINE_94_2 equ $
ok40: mov al, 35
myjmp TWINE_94_1
TWINE_80_15 equ $
jz ok39
 add ax, 39
myjmp TWINE_80_14
TWINE_93_0 equ $
ret
TWINE_94_1 equ $
mov cl, 7
myjmp TWINE_94_0
TWINE_80_14 equ $
fallthrough39:xor al, 45^255
myjmp TWINE_80_13
TWINE_94_0 equ $
ret
TWINE_80_13 equ $
inc al
myjmp TWINE_80_12
TWINE_95_2 equ $
ok101: mov al, 61
myjmp TWINE_95_1
TWINE_80_12 equ $
jz ok45
myjmp TWINE_80_11
TWINE_95_1 equ $
mov cl, 7
myjmp TWINE_95_0
TWINE_96_2 equ $
ok52: mov al, 16
myjmp TWINE_96_1
TWINE_80_11 equ $
dec al
myjmp TWINE_80_10
TWINE_95_0 equ $
ret
TWINE_96_1 equ $
mov cl, 6
myjmp TWINE_96_0
TWINE_80_10 equ $
xor al, 45^255
myjmp TWINE_80_9
TWINE_96_0 equ $
ret
TWINE_80_9 equ $
fallthrough45:rol al, 3
myjmp TWINE_80_8
TWINE_97_2 equ $
ok112: mov al, 82
myjmp TWINE_97_1
TWINE_80_8 equ $
cmp al, ((59<<3) | (59>>5)) & 255
myjmp TWINE_80_7
TWINE_97_1 equ $
mov cl, 7
myjmp TWINE_97_0
TWINE_98_2 equ $
ok85: mov al, 14
myjmp TWINE_98_1
TWINE_80_7 equ $
jz ok59
myjmp TWINE_80_6
TWINE_97_0 equ $
ret
TWINE_98_1 equ $
mov cl, 7
myjmp TWINE_98_0
TWINE_80_6 equ $
ror al, 3
myjmp TWINE_80_5
TWINE_98_0 equ $
ret
TWINE_80_5 equ $
fallthrough59:xor al, 62^255
myjmp TWINE_80_4
TWINE_99_2 equ $
ok43: mov al, 25
myjmp TWINE_99_1
TWINE_80_4 equ $
inc al
myjmp TWINE_80_3
TWINE_99_1 equ $
mov cl, 7
myjmp TWINE_99_0
TWINE_100_2 equ $
ok54: mov al, 83
myjmp TWINE_100_1
TWINE_80_3 equ $
jz ok62
myjmp TWINE_80_2
TWINE_99_0 equ $
ret
TWINE_100_1 equ $
mov cl, 7
myjmp TWINE_100_0
TWINE_80_2 equ $
dec al
myjmp TWINE_80_1
TWINE_100_0 equ $
ret
TWINE_80_1 equ $
xor al, 62^255
myjmp TWINE_80_0
TWINE_101_2 equ $
ok50: mov al, 22
myjmp TWINE_101_1
TWINE_80_0 equ $
fallthrough62:jmp twine17_start
TWINE_101_1 equ $
mov cl, 7
myjmp TWINE_101_0
TWINE_102_29 equ $
twine0_start: mov cl, 186
myjmp TWINE_102_28
TWINE_101_0 equ $
ret
TWINE_102_28 equ $
mov r8b, al
myjmp TWINE_102_27
TWINE_103_2 equ $
ok84: mov al, 52
myjmp TWINE_103_1
TWINE_102_27 equ $
mul cl
myjmp TWINE_102_26
TWINE_103_1 equ $
mov cl, 6
myjmp TWINE_103_0
TWINE_104_2 equ $
ok53: mov al, 94
myjmp TWINE_104_1
TWINE_102_26 equ $
cmp ax, 186*106
myjmp TWINE_102_25
TWINE_103_0 equ $
ret
TWINE_104_1 equ $
mov cl, 7
myjmp TWINE_104_0
TWINE_102_25 equ $
jz ok106
myjmp TWINE_102_24
TWINE_104_0 equ $
ret
TWINE_102_24 equ $
mov al, r8b
myjmp TWINE_102_23
TWINE_105_2 equ $
ok47: mov al, 97
myjmp TWINE_105_1
TWINE_102_23 equ $
fallthrough106:xor rcx, rcx
 mov cl, 87
myjmp TWINE_102_22
TWINE_105_1 equ $
mov cl, 7
myjmp TWINE_105_0
TWINE_106_2 equ $
ok88: mov al, 117
myjmp TWINE_106_1
TWINE_102_22 equ $
lp87: dec al
myjmp TWINE_102_21
TWINE_105_0 equ $
ret
TWINE_106_1 equ $
mov cl, 7
myjmp TWINE_106_0
TWINE_102_21 equ $
loop lp87
myjmp TWINE_102_20
TWINE_106_0 equ $
ret
TWINE_102_20 equ $
test al, al
myjmp TWINE_102_19
TWINE_107_2 equ $
ok79: mov al, 38
myjmp TWINE_107_1
TWINE_102_19 equ $
jz ok87
myjmp TWINE_102_18
TWINE_107_1 equ $
mov cl, 7
myjmp TWINE_107_0
TWINE_108_2 equ $
ok96: mov al, 121
myjmp TWINE_108_1
TWINE_102_18 equ $
add al, 87
myjmp TWINE_102_17
TWINE_107_0 equ $
ret
TWINE_108_1 equ $
mov cl, 7
myjmp TWINE_108_0
TWINE_102_17 equ $
fallthrough87:bt ax, 0
myjmp TWINE_102_16
TWINE_108_0 equ $
ret
TWINE_102_16 equ $
jnc fallthrough95
myjmp TWINE_102_15
TWINE_109_2 equ $
ok59: mov al, 105
myjmp TWINE_109_1
TWINE_102_15 equ $
bt ax, 1
myjmp TWINE_102_14
TWINE_109_1 equ $
mov cl, 7
myjmp TWINE_109_0
TWINE_110_2 equ $
ok94: mov al, 114
myjmp TWINE_110_1
TWINE_102_14 equ $
jnc fallthrough95
myjmp TWINE_102_13
TWINE_109_0 equ $
ret
TWINE_110_1 equ $
mov cl, 7
myjmp TWINE_110_0
TWINE_102_13 equ $
bt ax, 2
myjmp TWINE_102_12
TWINE_110_0 equ $
ret
TWINE_102_12 equ $
jnc fallthrough95
myjmp TWINE_102_11
TWINE_111_2 equ $
ok63: mov al, 10
myjmp TWINE_111_1
TWINE_102_11 equ $
bt ax, 3
myjmp TWINE_102_10
TWINE_111_1 equ $
mov cl, 7
myjmp TWINE_111_0
TWINE_112_2 equ $
ok86: mov al, 20
myjmp TWINE_112_1
TWINE_102_10 equ $
jnc fallthrough95
myjmp TWINE_102_9
TWINE_111_0 equ $
ret
TWINE_112_1 equ $
mov cl, 6
myjmp TWINE_112_0
TWINE_102_9 equ $
bt ax, 4
myjmp TWINE_102_8
TWINE_112_0 equ $
ret
TWINE_102_8 equ $
jnc fallthrough95
myjmp TWINE_102_7
TWINE_113_25 equ $
twine1_start: sub ah, ah
 sub ax, 52
myjmp TWINE_113_24
TWINE_102_7 equ $
bt ax, 5
myjmp TWINE_102_6
TWINE_113_24 equ $
jz ok52
 add ax, 52
myjmp TWINE_113_23
TWINE_114_2 equ $
ok110: mov al, 69
myjmp TWINE_114_1
TWINE_102_6 equ $
jc fallthrough95
myjmp TWINE_102_5
TWINE_113_23 equ $
fallthrough52:mov cl, 196
myjmp TWINE_113_22
TWINE_114_1 equ $
mov cl, 7
myjmp TWINE_114_0
TWINE_102_5 equ $
bt ax, 6
myjmp TWINE_102_4
TWINE_113_22 equ $
mov r8b, al
myjmp TWINE_113_21
TWINE_114_0 equ $
ret
TWINE_102_4 equ $
jnc fallthrough95
myjmp TWINE_102_3
TWINE_113_21 equ $
mul cl
myjmp TWINE_113_20
TWINE_102_3 equ $
jmp ok95
myjmp TWINE_102_2
TWINE_113_20 equ $
cmp ax, 196*57
myjmp TWINE_113_19
TWINE_115_2 equ $
ok95: mov al, 30
myjmp TWINE_115_1
TWINE_102_2 equ $
fallthrough95:xor al, 13
myjmp TWINE_102_1
TWINE_113_19 equ $
jz ok57
myjmp TWINE_113_18
TWINE_115_1 equ $
mov cl, 7
myjmp TWINE_115_0
TWINE_102_1 equ $
jz ok13
 xor al, 13
myjmp TWINE_102_0
TWINE_113_18 equ $
mov al, r8b
myjmp TWINE_113_17
TWINE_115_0 equ $
ret
TWINE_102_0 equ $
fallthrough13:jmp twine1_start
TWINE_113_17 equ $
fallthrough57:xor al, 65^255
myjmp TWINE_113_16
TWINE_113_16 equ $
inc al
myjmp TWINE_113_15
TWINE_116_2 equ $
ok33: mov al, 102
myjmp TWINE_116_1
TWINE_117_2 equ $
ok38: mov al, 126
myjmp TWINE_117_1
TWINE_113_15 equ $
jz ok65
myjmp TWINE_113_14
TWINE_116_1 equ $
mov cl, 7
myjmp TWINE_116_0
TWINE_117_1 equ $
mov cl, 7
myjmp TWINE_117_0
TWINE_113_14 equ $
dec al
myjmp TWINE_113_13
TWINE_116_0 equ $
ret
TWINE_117_0 equ $
ret
TWINE_113_13 equ $
xor al, 65^255
myjmp TWINE_113_12
TWINE_113_12 equ $
fallthrough65:cmp al, 70
myjmp TWINE_113_11
TWINE_118_2 equ $
ok93: mov al, 3
myjmp TWINE_118_1
TWINE_113_11 equ $
jz ok70
myjmp TWINE_113_10
TWINE_118_1 equ $
mov cl, 7
myjmp TWINE_118_0
TWINE_113_10 equ $
fallthrough70:xor rcx, rcx
 mov cl, 47
myjmp TWINE_113_9
TWINE_118_0 equ $
ret
TWINE_113_9 equ $
lp47: dec al
myjmp TWINE_113_8
TWINE_113_8 equ $
loop lp47
myjmp TWINE_113_7
TWINE_113_7 equ $
test al, al
myjmp TWINE_113_6
TWINE_113_6 equ $
jz ok47
myjmp TWINE_113_5
TWINE_113_5 equ $
add al, 47
myjmp TWINE_113_4
TWINE_113_4 equ $
fallthrough47:sub al, 124
myjmp TWINE_113_3
TWINE_113_3 equ $
test al, al
myjmp TWINE_113_2
TWINE_113_2 equ $
jz ok124
myjmp TWINE_113_1
TWINE_113_1 equ $
add al, 124
myjmp TWINE_113_0
TWINE_113_0 equ $
fallthrough124:jmp twine2_start
