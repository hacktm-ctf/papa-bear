; rbx = pointer inside banner
; rsi = pointer to user input
; r8, 

bits 64

global _start, ohno
extern twine0_start, rope_start

[section .vmp0 write noexec]
banner:
db "   ______   _______  ______   _______      ______   _______  _______  ______", 10
db "  (_____ \ (_______)(_____ \ (_______)    (____  \ (_______)(_______)(_____ \", 10
db "   _____) ) _______  _____) ) _______      ____)  ) _____    _______  _____) )", 10
db "  |  ____/ |  ___  ||  ____/ |  ___  |    |  __  ( |  ___)  |  ___  ||  __  /", 10
db "  | |      | |   | || |      | |   | |    | |__)  )| |_____ | |   | || |  \ \", 10
db "  |_|      |_|   |_||_|      |_|   |_|    |______/ |_______)|_|   |_||_|   |_|", 10
db 10
db "            dMMM=-        dMMMMMMMMMMMb  dMMMMMMMMMMMb        -=MMMb", 10
db "          dMMMP       dMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMb        qMMb", 10
db "          MMMMb   dMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMb    dMMM", 10
db "          qMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMP", 10
db "            QMMMMMMMMMMMMMMMMMMMMMMMMMP  QMMMMMMMMMMMMMMMMMMMMMMMMMMP", 10
db "              QMMMMMMMMMMMMMMMMMMMP          QMMMMMMMMMMMMMMMMMMMP", 10
db "                     QMMMMMMP                         QMMMMMMP", 10, 10
.end:

[section .vmp1 write exec]
init_rbx_if_zero:
	mov r8, rbx
	add rbx, 0x1038
	add r8, banner.end - banner
	ror rbx, 9
	cmp bh, 8
	mov rbx, banner.end
	cmovz rbx, r8
	add rbx, banner - banner.end
	mov byte [init_rbx_if_zero], 0xc3
	ret


_start:
	mov rsi, [rsp+16]
	mov rbx, banner
	call loop_inputbytes

putbit_al:
	call init_rbx_if_zero
	.leac3: lea r9, [rax + 0xc300]
	mov al, 'M'
	xchg rbx, rdi
	mov rcx, banner.end
	sub rcx, rdi
	repne scasb
	lea rdx, [rax + 10]	; 'M' + 10 == 'W'
	test r9b, 1
	cmovnz rax, rdx
	jrcxz done
	dec rdi
	stosb
	xchg rbx, rdi
	jmp .leac3+4	; c3 byte of `lea r9, [rax + 0xc300]`

loop_inputbytes:
	pop r8
	lodsb
	test al, al
	jz done
	call twine0_start
	; al = bits
	; cl = numbits
	movzx rcx, cl
	.L0:
		mov r11, rcx
		mov r10, rax
		and al, 1
		call putbit_al
		mov rcx, r11
		mov rax, r10
		shr rax, 1
		loop .L0
	call loop_inputbytes

done:
	; write(0, banner, sizeof(banner))
	mov rdx, banner.end - banner
	mov rsi, banner
	mov rdi, 0
	mov rax, 1
	syscall

ohno:
	; exit(0)
	mov rdi, 0
	mov rax, 60
	syscall
