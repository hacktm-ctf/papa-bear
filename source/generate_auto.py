from collections import Counter
import random
from string import printable as alphabet

flag = "HackTM{F4th3r bEaR s@y$: Smb0DY Ea7 My Sb3VE}"

fatass = list(flag + alphabet)
random.shuffle(fatass)

# (cost, value) or (cost, (sub0, sub1))
codes = [(i[1], i[0]) for i in Counter(fatass).items()]
while len(codes) > 1:
	codes.sort(key=lambda a: a[0])
	c0 = codes.pop(0)
	c1 = codes.pop(0)
	codes.append((c0[0] + c1[0], (c0, c1)))

dc = {}

def pc(code, prefix=''):
	if type(code[1]) == tuple:
		pc(code[1][0], prefix+'0')
		pc(code[1][1], prefix+'1')
	else:
		dc[code[1]] = prefix
		print(prefix, repr(code[1]))

pc(codes[0])

print(dc)

flag_enc = ""
for c in flag[:-1]:
	flag_enc += dc[c]

# TODO 20 checkers
# Usable regs: rax, rcx, rdx, rdi, r8-r15



def gen_bitscan_forward(value):
	s = "xor ah, ah\nmov r8w, ax\n"
	while value:
		sk = 0
		while value & (1<<sk) == 0: sk += 1
		s += """
bsf cx, r8w
shr r8w, {sk}+1
cmp cx, {sk}
jnz {notok}
""".format(sk=sk, notok="{notok}")
		value >>= sk+1
	s += """test r8w, r8w\njz {ok}"""
	return s


def gen_bt(value):
	s = ""
	for i in range(7):
		s += f"bt ax, {i}\n"
		if value & (1<<i):
			s += "jnc {notok}\n"
		else:
			s += "jc {notok}\n"
	s += "jmp {ok}"
	return s

def gen_crc32(value):
	assert(32 <= value < 127)
	crc32_lut = [ 549293790, 3537243613, 3246849577, 871202090, 3878099393, 357341890, 102525238, 4101499445, 2858735121, 1477399826, 1264559846, 3107202533, 1845379342, 2677391885, 2361733625, 2125378298, 820201905, 3263744690, 3520608582, 598981189, 4151959214, 85089709, 373468761, 3827903834, 3124367742, 1213305469, 1526817161, 2842354314, 2107672161, 2412447074, 2627466902, 1861252501, 1098587580, 3004210879, 2688576843, 1378610760, 2262928035, 1955203488, 1742404180, 2511436119, 3416409459, 969524848, 714683780, 3639785095, 205050476, 4266873199, 3976438427, 526918040, 1361435347, 2739821008, 2954799652, 1114974503, 2529119692, 1691668175, 2005155131, 2247081528, 3690758684, 697762079, 986182379, 3366744552, 476452099, 3993867776, 4250756596, 255256311, 1640403810, 2477592673, 2164122517, 1922457750, 2791048317, 1412925310, 1197962378, 3037525897, 3944729517, 427051182, 170179418, 4165941337, 746937522, 3740196785, 3451792453, 1070968646, 1905808397, 2213795598, 2426610938, 1657317369, 3053634322, 1147748369, 1463399397, 2773627110, 4215344322, 153784257, 444234805, 3893493558, 1021025245, 3467647198, 3722505002]
	return """
	xor rcx, rcx; xchg rax, rdx
	mov rax, {crc32_value}
	xchg rax, rdx
	crc32 rcx, al
	cmp rcx, rdx; jz {ok}
	""".format(crc32_value=crc32_lut[value-32], ok="{ok}")

checkers = [
"""
cmp al, {value}
jz {ok}
""",
"""
mov cl, 255^{value}
xor cl, al
inc cl
test cl, cl
jz {ok}
""",
"""
sub al, {value}
test al, al
jz {ok}
add al, {value}
""",

#  01001000
#&~01011001
#  00000000 ok

#! 10110111
#| 01011001

# a b a!b b!a
# 0 0  0   0
# 0 1  0   1
# 1 0  1   0
# 1 1  0   0
# 
# b!a == 0 <=> !b + a == 1

# x == c <=> x&!c == 0 && !x&c == 0

"""
test al, 255^{value}
jnz {notok}
mov r8b, al
or al, 255^{value}
cmp al, 255
jz {ok}
mov al, r8b
""",

"""
xor al, {value}
jz {ok}; xor al, {value}
""",

"""
xor al, {value}^255
inc al
jz {ok}
dec al
xor al, {value}^255
""",
"""
add al, 256-{value}
jz {ok}; add al, {value}
""",
"""
sub ah, ah; sub ax, {value}
jz {ok}; add ax, {value}
""",
"""
xor rcx, rcx; mov cl, {value}
lp{value}: dec al
loop lp{value}
test al, al
jz {ok}
add al, {value}
""",
"""
rol al, 3
cmp al, (({value}<<3) | ({value}>>5)) & 255
jz {ok}
ror al, 3
""",
# ^ first 10

gen_bitscan_forward,
gen_bt,
gen_crc32,
"""
mov cl, {rand1}
mov r8b, al
mul cl
cmp ax, {rand1}*{value}
jz {ok}
mov al, r8b
"""
]

def setter1(bits):
	return f"""mov al, {int(bits[::-1], 2)}
mov cl, {len(bits)}
ret"""

setters = [setter1]

fibers = []

# Create checker fibers
for c in alphabet:
	checker = random.choice(checkers)
	print(f"{repr(c)} is checked by checker #{checkers.index(checker)}")
	if type(checker) != str:
		checker = checker(ord(c))
	checker = checker.strip()
	checker += f"\nfallthrough{ord(c)}:"
	fiber = checker.format(value=ord(c), rand1=random.randint(0, 255), ok=f'ok{ord(c)}', notok=f'fallthrough{ord(c)}').split("\n")
	fiber = [a.strip() for a in fiber]
	fibers.append(fiber)

# Create twines
num_twines = 19
twines = [[] for _ in range(num_twines)]

for f in fibers:
	Ti = random.randrange(len(twines))
	if len(twines[Ti]) and twines[Ti][-1].startswith("fallthrough"):
		twines[Ti][-1] += f[0]
		twines[Ti] += f[1:]
	else:
		twines[Ti] += f


for Ti in range(len(twines)):
	twines[Ti][0] = f"twine{Ti}_start: " + twines[Ti][0]
	if Ti != num_twines-1:
		twines[Ti][-1] += f"jmp twine{Ti+1}_start"
	else:
		twines[Ti][-1] += "jmp ohno"

# Create setter fibers -> each in its own twine
for c in alphabet:
	setter = f"ok{ord(c)}: " + random.choice(setters)(dc[c])
	twines.append(setter.split("\n"))

random.shuffle(twines)

current_twines = [0, 1, 2]

asm = ""

def asmline(a):
	global asm
	#print(a)
	asm += a + "\n"

while len(current_twines):
	Ti = current_twines[0]
	current_twines = current_twines[1:] + current_twines[:1]

	if Ti >= len(twines): continue

	if min(current_twines) >= len(twines): break

	if len(twines[Ti]) == 0:
		n = max(current_twines)+1
		current_twines = current_twines[:-1]
		if n < len(twines): current_twines.append(n)
		continue

	if min(current_twines) >= len(twines): break

	for T in current_twines:
		if T < len(twines) and len(twines[T]): break
	else:
		break

	line = twines[Ti].pop(0)
	asmline(f"TWINE_{Ti}_{len(twines[Ti])} equ $\n{chr(10).join(line.split(';'))}")
	if len(twines[Ti]) != 0:
		asmline(f"myjmp TWINE_{Ti}_{len(twines[Ti])-1}")

with open("auto.asm", "w") as fasm:
	fasm.write("""
section .vmp2 nowrite exec
global twine0_start
extern ohno

%macro myjmp 1
	jmp %1
	align 2
%endmacro

""")
	fasm.write(asm)

import os

print("nasm -felf64 auto.asm -o auto.o")
os.system("nasm -felf64 auto.asm -o auto.o")

with open("auto.o", "rb") as fcode:
	code = bytearray(fcode.read())

jmp8_replacements = [
# movabs r10-r15, imm64
b"\x49\xba", b"\x49\xbb", b"\x49\xbc", b"\x49\xbd", b"\x49\xbe", b"\x49\xbf"
]

j8cnt = 0
while (pos := code.find(b"\xeb\x08")) != -1:
	j8cnt += 1
	code[pos:pos+2] = random.choice(jmp8_replacements)

print(f"{j8cnt} jump +8 replaced with movabs")

rrcnt = 0
while (pos := code.find(b"\xc3\xc3")) != -1:
	rrcnt += 1
	code[pos:pos+2] = b"\x90\xc3"

print(f"{rrcnt} ret;ret replaced with nop;ret")

with open("auto.o", "wb") as fcode:
	fcode.write(code)

print(f"Saved patcged auto.o")

print("Expected huffman coding:")
for c in flag:
	print(f"{dc[c]}: al={int(dc[c][::-1],2)}, cl={len(dc[c])})")
